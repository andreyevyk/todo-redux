
const initialState={
   todoes: []
};

export default function todoList(state = initialState, action){
   switch(action.type){
      case 'ADD_TODO':
         return {
            ...state,
            todoes: [
               ...state.todoes,
               {id: state.todoes.length + 1, active:true, text:action.text }
            ]
         };
      case 'TOGGLE_TODO':
         let pos = state.todoes.map( item => item.id ).indexOf(action.id)
         return {
            ...state,
            todoes: [
               ...state.todoes.map((item, index) => {
                  if (index !== pos) {
                    return item
                  }            
                  item.active = !item.active;
                  return item;            
                })                            
            ]
         };      
      case 'REMOVE_TODO':
         return {...state, todoes: [...state.todoes.filter(item => item.id !== action.id)]};
         
      default:
         return state;
   }
}