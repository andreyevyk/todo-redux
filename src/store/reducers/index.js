import {combineReducers} from 'redux';
import toDoList from './todoList';

export default combineReducers({
   toDoList,
})