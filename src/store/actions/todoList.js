export function addToDo(text){
   return {
      type: 'ADD_TODO',
      text,
   }
}

export function toggleToDo(id){
   return {
      type: 'TOGGLE_TODO',
      id,
   }
}

export function removeToDo(id){
   return {
      type: 'REMOVE_TODO',
      id,
   }
}