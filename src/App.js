import React from 'react';
import { Provider } from "react-redux";

import store from "./store";

import './App.css';
import ToDoList from './components/ToDoList';


function App() {
  return (
    <div className="App">
      <Provider store={store} >
         <ToDoList/>
      </Provider>
    </div>
  );
}

export default App;
