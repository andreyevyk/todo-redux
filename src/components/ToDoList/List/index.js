import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { bindActionCreators } from 'redux'

import * as TodoActions from '../../../store/actions/todoList'


function Insert({todoes,toggleToDo, removeToDo}){
   return(
      todoes.map((todo,index) => (
         <Div key={todo.id}>
            {todo.active ? <Span>{todo.text}</Span> : <Span className="lineThrough">{todo.text}</Span>}
            <div >
               <Button onClick={() => toggleToDo(todo.id)}>Toggle</Button>
               <Button onClick={() => removeToDo(todo.id)}>Remove</Button>
            </div>
         </Div>
      ))
      
   )
}

const Button = styled.button`
   background: none;
   border: none;
   color: white;
   cursor: pointer;
   font-size: 0.8em;
   font-weight:bold;

   &:hover {
      font-size: 0.9em;
   }
`;

const Span = styled.span`
   font-size: 1.2em
`;
const Div = styled.div`
   display: flex ;
   padding-right: 30px;
   padding-left: 30px;
   padding-bottom: 10px;
   color: white;
   justify-content: space-between;
   flex-direction: row;
   
`;
const mapDispatchToProps = dispatch =>
  bindActionCreators(TodoActions, dispatch);

const mapStateToProps = state => ({
   todoes: state.toDoList.todoes
});

export default connect(mapStateToProps,mapDispatchToProps)(Insert);