import React from 'react';
import Insert from './Insert';
import List from './List';

function ToDoList(){
   return(
      <>
         <div className="insert">
            <Insert/>
         </div>
         <div className="list">
            <List/>
         </div>
      </>
      
   )
}

export default ToDoList;