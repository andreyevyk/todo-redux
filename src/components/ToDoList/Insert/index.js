import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'


import * as TodoActions from '../../../store/actions/todoList'

function Insert({addToDo}){
   const [value, setValue] = useState('');

   return(
      <>
         <Input value={value} onChange={e => setValue( e.target.value)}/>
         <Button onClick={() => addToDo(value)}>Novo</Button>
      </>
   )
}

const Input =  styled.input`
   padding-left:10px;
   height: 30px;
   width: 75%;
   margin-right: 5%;
   border-radius: 3px;
   border: none;
`;
const Button = styled.button`
   height: 30px;
   width: 20%;
   color: white;
   background-color: #11bd6a;
   cursor: pointer;
   border-radius: 3px;
   border: none;
`

const mapDispatchToProps = dispatch =>
  bindActionCreators(TodoActions, dispatch);

 

export default connect(null,mapDispatchToProps)(Insert);